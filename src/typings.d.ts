/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
}
declare module 'jsQR' {
  export default function jsQR(imageData: Uint8ClampedArray, width: number, height: number): {
        data: string
  } | null;
}
declare module qrcode {

  function decode(url?: any);

  var callback: (result: string) => void;
}
