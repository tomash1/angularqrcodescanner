import { Component, OnInit, Input, AfterViewInit, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { QRScannerService } from '../qr-scanner-impl/QRScannerService';
import { ActivatedRoute } from '@angular/router';
import { QRLibrary } from '../enums/QRLibrary';
import { QRScanner } from '../qr-scanner-impl/QRScanner';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-qr-scanner',
  templateUrl: './qr-scanner.component.html',
  styleUrls: ['./qr-scanner.component.scss']
})
export class QrScannerComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('qrCodeCanvas') qrCodeCanvas: ElementRef;
  @ViewChild('qrCodeVideo') qrCodeVideo: ElementRef;

  private selectedLibrary: string;
  private scanner: QRScanner;
  private canvasRenderingContext: CanvasRenderingContext2D;
  private isUserMediaLoaded = false;
  private capturePhotoIntervalId: number;
  private decodePhotoIntervalId: number;

  private qrCodeCanvasElement: HTMLCanvasElement;
  private qrCodeVideoElement: HTMLVideoElement;
  private navigator: any;
  private decodedValue: string;
  protected videoStream: MediaStream;
  private scannerDecodedSubscription: Subscription;

  public get DecodedValue(): string {
    return this.decodedValue != null && this.decodedValue !== '' ? this.decodedValue : 'Nothing decoded';
  }

  public get SelectedLibraryName(): string {
    return this.selectedLibrary != null ? this.selectedLibrary : 'not given';
  }

  public get IsVideoSupported(): boolean {
    return !!(this.qrCodeVideo && this.qrCodeVideo.nativeElement);
  }

  public get IsCanvasSupported(): boolean {
    return !!(this.qrCodeCanvas && this.qrCodeCanvas.nativeElement && this.qrCodeCanvas.nativeElement.getContext('2d'));
  }

  public get IsScannerAvailable(): boolean {
    return this.scanner != null;
  }

  public get IsUserMediaSupported(): boolean {
    return !!(this.navigator.getUserMedia
      || this.navigator.webkitGetUserMedia
      || this.navigator.mozGetUserMedia
      || this.navigator.msGetUserMedia);
  }

  constructor(private qrScannerService: QRScannerService, private route: ActivatedRoute) {
    this.route.params.subscribe(res => {
      this.selectedLibrary = res.library;
      this.tryInitializeQRCodeService();
    });

    this.navigator = navigator;
  }

  private getQrLibraryFromString(libraryName: string): QRLibrary {
    switch (libraryName) {
      case 'jsqr': return QRLibrary.JSQR;
      case 'jsqrcode': return QRLibrary.JSQRCode;
      case 'zxing': return QRLibrary.ZXING;
      default: return null;
    }
  }

  ngOnInit() {

  }

  ngAfterViewInit(): void {
    this.qrCodeVideoElement = this.qrCodeVideo.nativeElement;
    this.qrCodeCanvasElement = this.qrCodeCanvas.nativeElement;
    this.setupComponent();
    this.tryInitializeQRCodeService();
  }

  private tryInitializeQRCodeService(): void {
    if (this.IsVideoSupported && this.IsCanvasSupported && this.selectedLibrary != null) {
      this.qrScannerService.QRCanvasElement = this.qrCodeCanvasElement;
      this.qrScannerService.QRVideoElement = this.qrCodeVideoElement;
      this.scanner = this.qrScannerService.getScanner(this.getQrLibraryFromString(this.selectedLibrary));
      if (this.scanner != null) {
        this.scannerDecodedSubscription = this.scanner
          .getDecoded()
          .subscribe((decodedValue: string) => {
            this.decodedValue = decodedValue;
          });
      }
    }
  }

  ngOnDestroy(): void {
    if (this.scannerDecodedSubscription != null) {
      this.scannerDecodedSubscription.unsubscribe();
    }

    if (this.videoStream) {
      const tracks: MediaStreamTrack[] = this.videoStream.getVideoTracks();
      if (tracks != null && tracks.length > 0) {
        tracks.forEach((track) => {
          track.stop();
        });
      }

      this.videoStream = null;
    }

    if (this.decodePhotoIntervalId != null) {
      clearInterval(this.decodePhotoIntervalId);
    }
    if (this.capturePhotoIntervalId != null) {
      clearInterval(this.capturePhotoIntervalId);
    }

    if (this.scanner != null) {
      this.scanner.destroy();
    }
  }

  private setupComponent(): void {
    this.canvasRenderingContext = this.qrCodeCanvasElement.getContext('2d');
    if (this.canvasRenderingContext) {
      this.canvasRenderingContext.clearRect(0, 0, this.qrCodeCanvasElement.width, this.qrCodeCanvasElement.height);
    }

    this.qrCodeVideoElement.width = window.innerWidth < 600 ? window.innerWidth - 75 : 600;

    if (this.navigator.mediaDevices && this.IsUserMediaSupported) {
      this.tryActivateStreamFromDevice();
    } else {
      console.log('mediaDevices not available');
    }
  }

  protected tryActivateStreamFromDevice(): void {
    this.navigator.mediaDevices
      .enumerateDevices()
      .then(value => this.gotDevices(value),
        reason => {
          this.video_onError();
        });
  }

  private gotDevices(devicesInfo: MediaDeviceInfo[]): void {
    this.stopActivatedVideoStream();
    const constraints: any = {
      video: { facingMode: 'environment' },
      audio: false
    };

    if (this.navigator.mediaDevices.getUserMedia) {
      this.navigator.mediaDevices
        .getUserMedia(constraints)
        .then(
          value => {
            this.video_onSuccess(value);
          },
          reason => this.video_onError()
        );
    } else {
      console.log('navigator.mediaDevices has no getUserMedia method. Cannot activate stream from device');
      this.video_onError();
    }
  }

  private stopActivatedVideoStream(): void {
    if (this.videoStream) {
      this.videoStream
        .getTracks()
        .forEach((track) => {
          track.stop();
        });
    }
  }

  protected video_onSuccess(stream: any): void {

    this.videoStream = stream;
    this.qrCodeVideoElement.srcObject = stream;

    this.isUserMediaLoaded = true;

    this.capturePhoto();
    this.decodePhoto();
  }

  protected video_onError(): void {
    this.isUserMediaLoaded = false;
    console.log('Video error!');
  }

  private capturePhoto(): void {

    if (this.capturePhotoIntervalId != null) {
      clearInterval(this.capturePhotoIntervalId);
    }

    this.capturePhotoIntervalId = setInterval(() => {
      this.capturePhotoInternal();
    }, 1000 / 10);
  }

  private decodePhoto(): void {

    if (this.decodePhotoIntervalId != null) {
      clearInterval(this.decodePhotoIntervalId);
    }

    this.decodePhotoIntervalId = setInterval(() => {
      this.decodePhotoInternal();
    }, 500);
  }

  private decodePhotoInternal(): void {
    if (this.isUserMediaLoaded) {
      try {
        if (this.canvasRenderingContext && this.qrCodeVideoElement.videoWidth !== 0 && this.qrCodeVideoElement.videoHeight !== 0) {
          this.scanner.decode();
        }
      } catch (ex) {
        console.log('DECODE ERROR !');
      }
    }
  }

  private capturePhotoInternal(): void {
    if (this.isUserMediaLoaded) {
      if ((this.qrCodeVideoElement.videoWidth !== 0 && this.qrCodeVideoElement.videoHeight !== 0)
        && (this.qrCodeVideoElement.videoWidth !== this.qrCodeCanvasElement.width
          || this.qrCodeVideoElement.videoHeight !== this.qrCodeCanvasElement.height)) {
        this.qrCodeCanvasElement.width = this.qrCodeVideoElement.videoWidth;
        this.qrCodeCanvasElement.height = this.qrCodeVideoElement.videoWidth;
      }
      const w: number = this.qrCodeVideoElement.videoWidth;
      const h: number = this.qrCodeVideoElement.videoHeight;
      let r: number = h - w;
      r = r / 2;

      this.canvasRenderingContext.drawImage(
        this.qrCodeVideoElement,
        0, r,
        this.qrCodeVideoElement.videoWidth, this.qrCodeVideoElement.videoWidth,
        0, 0,
        this.qrCodeVideoElement.videoWidth, this.qrCodeVideoElement.videoWidth);
      // let imageData: ImageData = 
      // imageData = this.convertImageDataToGrayScale(imageData);
      // // overwrite original image
      // this.canvasContext.putImageData(imageData, 0, 0);
    }
  }
}
