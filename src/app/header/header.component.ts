import { Component, OnInit } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  private title: string = "Home";

  public get Title(): string {
    return this.title;
  }

  constructor(private router: Router) {
    router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        let urlSplit: string[] = event.url.split("/");
        let title: string = urlSplit.pop();
        this.title = title != null && title.replace(/\s/g,'') != "" ? title : "Home";
      }
    });
  }

  ngOnInit() {
  }

}
