import { QRScanner } from './QRScanner';

export class JSQRCodeScanner extends QRScanner {

    constructor(qrCanvas: HTMLCanvasElement, qrVideo: HTMLVideoElement) {
        super(qrCanvas, qrVideo);
        qrcode.callback = (foundQr: string) => this.onDecoded(foundQr);
    }

    decode(): void {
        const qrCanvasElementUrl: string = this.qrCanvas.toDataURL();
        qrcode.decode(qrCanvasElementUrl);
    }

    destroy(): void {}
}
