import { Subject } from 'rxjs/Subject';

export interface IQRScanner {
    decode(): void;
    destroy(): void;
}

export abstract class QRScanner implements IQRScanner {
    decoded$: Subject<string> = new Subject<string>();

    abstract decode(): void;
    abstract destroy(): void;
    constructor(protected qrCanvas: HTMLCanvasElement, protected qrVideo: HTMLVideoElement) {}

    public onDecoded(decoded: string): void {
        this.decoded$.next(decoded);
    }

    public getDecoded(): Subject<string> {
        return this.decoded$;
    }

    protected getCanvas2DContext(): CanvasRenderingContext2D {
        return this.qrCanvas.getContext('2d');
    }
}
