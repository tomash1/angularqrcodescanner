import { Injectable } from "@angular/core";
import { JSQRScanner } from "./JSsqrScanner";
import { QRLibrary } from "../enums/QRLibrary";
import { QRScanner } from "./QRScanner";
import { JSQRCodeScanner } from "./JSQRCodeScanner";
import { ZXingQRCodeScanner } from "./ZXINGQRCodeScanner";

@Injectable()
export class QRScannerService {

    private canvasElement: HTMLCanvasElement;
    private videoElement: HTMLVideoElement;

    constructor() { }

    public set QRCanvasElement(htmlElement: HTMLCanvasElement) {
        this.canvasElement = htmlElement;
    }

    public set QRVideoElement(htmlElement: HTMLVideoElement) {
        this.videoElement = htmlElement;
    }

    public getScanner(qrLibrary: QRLibrary): QRScanner {
        switch (qrLibrary) {
            case QRLibrary.JSQR: return new JSQRScanner(this.canvasElement, this.videoElement);
            case QRLibrary.JSQRCode: return new JSQRCodeScanner(this.canvasElement, this.videoElement);
            case QRLibrary.ZXING: return new ZXingQRCodeScanner(this.canvasElement, this.videoElement);
            default: return null;
        }
    }
}
