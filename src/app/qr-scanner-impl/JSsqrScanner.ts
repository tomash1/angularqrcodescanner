import jsqr from 'jsQR';
import { QRScanner } from './QRScanner';

export class JSQRScanner extends QRScanner {

    decode(): void {
        const imageData: ImageData = this.getCanvas2DContext().getImageData(
            0, 0,
            this.qrVideo.videoWidth, this.qrVideo.videoWidth
        );

        if (!(imageData instanceof ImageData)) {
            throw new TypeError('JSQRScanner decode accepts only ImageData');
        }

        const qr: any = jsqr(imageData.data, imageData.width, imageData.height);
        if (qr != null) {
            this.onDecoded(qr.data);
        }
    }

    destroy(): void {}
}
