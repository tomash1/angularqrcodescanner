import { QRScanner } from './QRScanner';
import { BrowserQRCodeReader, VideoInputDevice } from 'zxing-typescript/src/browser/BrowserQRCodeReader'
import Result from 'zxing-typescript/src/core/Result';

export class ZXingQRCodeScanner extends QRScanner {
    private reader: BrowserQRCodeReader;
    private isDecoding = false;

    constructor(qrCanvas: HTMLCanvasElement, qrVideo: HTMLVideoElement) {
        super(qrCanvas, qrVideo);
        this.reader = new BrowserQRCodeReader(200);
    }

    decode(): void {
        if (!this.isDecoding) {
            this.isDecoding = true;
            try {
                this.reader.decodeFromInputVideoDevice(null).then((value: Result) => {
                    this.onDecoded(value.getText());
                    this.isDecoding = false;
                });
            } catch (ex) { }
        }
    }

    destroy(): void {
        this.reader.reset();
        this.reader = null;
        this.qrVideo.src = null;
        this.qrVideo.srcObject = null;
        this.isDecoding = false;
    }
}
