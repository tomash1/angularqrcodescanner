import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { QRScannerService } from './qr-scanner-impl/QRScannerService';
import { QrScannerComponent } from './qr-scanner/qr-scanner.component';
import { HeaderComponent } from './header/header.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { QrScannerModule } from './qr-scanner/qr-scanner.module';

@NgModule({
  declarations: [
    AppComponent, HeaderComponent, WelcomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    QrScannerModule
  ],
  providers: [QRScannerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
