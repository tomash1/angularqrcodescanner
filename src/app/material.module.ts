import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatMenuModule, MatIconModule } from '@angular/material';

@NgModule({
  imports: [MatButtonModule, MatMenuModule, MatIconModule],
  exports: [MatButtonModule, MatMenuModule, MatIconModule],
})
export class MaterialModule { }