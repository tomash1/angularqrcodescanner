export enum QRLibrary {
    JSQR = 0,
    JSQRCode = 1,
    ZXING = 2
}
